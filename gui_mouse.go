package main

import (
	"path"
	"strings"
	"time"

	"github.com/faiface/beep/speaker"
	"github.com/gdamore/tcell/v2"
	"gitlab.com/tslocum/cview"
)

func handleMouse(event *tcell.EventMouse, action cview.MouseAction) (*tcell.EventMouse, cview.MouseAction) {
	mouseX, mouseY := event.Position()
	if action == cview.MouseLeftClick && mouseY == screenHeight-1 {
		if mouseX >= seekStart && mouseX <= seekEnd {
			if strings.ToLower(path.Ext(playingFileName)) == ".flac" {
				statusText = "Seeking FLAC files is unsupported"
				go func() {
					time.Sleep(5 * time.Second)
					statusText = ""
					go app.QueueUpdateDraw(updateMain)
				}()
				go app.QueueUpdateDraw(updateMain)

				return nil, 0
			}

			audioLock.Lock()
			speaker.Lock()

			if playingStreamer == nil {
				speaker.Unlock()
				audioLock.Unlock()
				return nil, 0
			}

			pos := float64(mouseX-seekStart) / float64(seekEnd-seekStart)
			if pos > 1 {
				pos = 1
			}
			seekTo := int(float64(playingStreamer.Len()-1) * pos)
			_ = playingStreamer.Seek(seekTo) // Ignore seek errors
			speaker.Unlock()
			audioLock.Unlock()

			go app.QueueUpdateDraw(updateStatus)
			return nil, 0
		} else if mouseX >= volumeStart && mouseX <= volumeEnd+1 {
			if mouseX > volumeEnd {
				mouseX = volumeEnd
			}

			if mouseX-volumeStart <= 3 {
				audioLock.Lock()
				speaker.Lock()

				if volume == nil {
					speaker.Unlock()
					audioLock.Unlock()
					return nil, 0
				}

				volume.Silent = !volume.Silent
				speaker.Unlock()
				audioLock.Unlock()

				go app.QueueUpdateDraw(updateStatus)
			} else {
				vol := -7.5 + float64(7.5)*(float64(mouseX-volumeStart-3)/float64(volumeEnd-volumeStart-3))
				if vol < -7.0 {
					vol = -7.0
				}
				setVolume(roundUnit(vol, 0.5))

				go app.QueueUpdateDraw(updateStatus)
			}
			return nil, 0
		}
	}

	return event, action
}
