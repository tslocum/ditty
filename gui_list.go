package main

import (
	"os"
	"path"
	"time"

	"github.com/gdamore/tcell/v2"
	"gitlab.com/tslocum/cview"
)

func listPrevious() {
	if queueFocused {
		queuePrevious()
		return
	}

	mainList.Transform(cview.TransformPreviousItem)
	go app.QueueUpdateDraw(updateLists)
}

func listNext() {
	if queueFocused {
		queueNext()
		return
	}

	mainList.Transform(cview.TransformNextItem)
	go app.QueueUpdateDraw(updateLists)
}

func queuePrevious() {
	queueList.Transform(cview.TransformPreviousItem)
	go app.QueueUpdateDraw(updateQueue)
}

func queueNext() {
	queueList.Transform(cview.TransformNextItem)
	go app.QueueUpdateDraw(updateQueue)
}

func listSelectCurrent() {
	if queueFocused {
		go queueSelect(queueList.GetCurrentItemIndex())
	} else {
		go listSelect(mainList.GetCurrentItemIndex())
	}
}

func listSelect(cursor int) {
	if cursor == 0 {
		browseFolder(path.Join(mainDirectory, ".."))
		return
	}

	if cursor < 0 || cursor-1 > len(mainFiles) {
		return
	}

	entry := mainFiles[cursor-1]
	if entry.File.IsDir() || entry.File.Mode()&os.ModeSymlink != 0 {
		browseFolder(entry.RealPath)
		return
	}

	audioFile, err := openFile(entry.RealPath, entry.Metadata)
	if err != nil {
		statusText = err.Error()
		go func() {
			time.Sleep(5 * time.Second)
			statusText = ""
			go app.QueueUpdateDraw(updateMain)
		}()
		go app.QueueUpdateDraw(updateMain)
		return
	}
	go play(audioFile)

	go app.QueueUpdateDraw(updateStatus)
}

func queueSelect(cursor int) {
	if cursor < 0 || cursor > len(queueFiles) {
		return
	}

	entry := queueFiles[cursor]
	if entry.File.IsDir() || entry.File.Mode()&os.ModeSymlink != 0 {
		return
	}

	audioFile, err := openFile(entry.RealPath, entry.Metadata)
	if err != nil {
		statusText = err.Error()
		go func() {
			time.Sleep(5 * time.Second)
			statusText = ""
			go app.QueueUpdateDraw(updateMain)
		}()
		go app.QueueUpdateDraw(updateMain)
		return
	}
	go play(audioFile)

	go app.QueueUpdateDraw(updateStatus)
}

func listQueue() {
	if mainList.GetCurrentItemIndex() == 0 {
		// TODO Show error
		return
	}

	entry := selectedMainEntry()
	if entry == nil {
		return
	} else if entry.File.IsDir() || entry.File.Mode()&os.ModeSymlink != 0 {
		scanFiles := scanFolderRecursively(entry.RealPath)

		queueLock.Lock()
		queueFiles = append(queueFiles, scanFiles...)
		queueLock.Unlock()

		if !disableAutoplay && playingFileID == 0 && len(queueFiles) > 0 {
			queueSelect(queueList.GetCurrentItemIndex())
		}
		go app.QueueUpdateDraw(updateQueue)
		return
	}

	queueLock.Lock()
	defer queueLock.Unlock()

	queueFiles = append(queueFiles, entry)

	if !disableAutoplay && playingFileID == 0 {
		queueSelect(queueList.GetCurrentItemIndex())
	}
	go app.QueueUpdateDraw(updateLists)
}

func listDelete() {
	if !queueFocused {
		return
	}

	queueLock.Lock()
	defer queueLock.Unlock()

	cursor := queueList.GetCurrentItemIndex()

	if cursor < 0 || len(queueFiles) <= cursor {
		return
	}

	queueFiles = append(queueFiles[:cursor], queueFiles[cursor+1:]...)

	go app.QueueUpdateDraw(updateQueue)
}

func selectedMainEntry() *libraryEntry {
	cursor := mainList.GetCurrentItemIndex()
	if cursor < 0 || cursor-1 > len(mainFiles) {
		return nil
	}

	return mainFiles[cursor-1]
}

func offsetMainEntry(offset int) *libraryEntry {
	cursor := mainList.GetCurrentItemIndex()
	if (cursor-1)+offset < 0 || (cursor-1)+offset >= len(mainFiles) {
		return nil
	}
	return mainFiles[(cursor-1)+offset]
}

func selectedQueueEntry() *libraryEntry {
	cursor := queueList.GetCurrentItemIndex()
	if cursor < 0 || cursor-1 > len(queueFiles) {
		return nil
	}

	return queueFiles[cursor]
}

func offsetQueueEntry(offset int) *libraryEntry {
	cursor := queueList.GetCurrentItemIndex()
	if cursor+offset < 0 || cursor+offset >= len(queueFiles) {
		return nil
	}

	return queueFiles[cursor+offset]
}

func listPreviousPage() {
	if !queueFocused {
		mainList.Transform(cview.TransformPreviousPage)
		go app.QueueUpdateDraw(updateMain)
	} else {
		queueList.Transform(cview.TransformPreviousPage)
		go app.QueueUpdateDraw(updateQueue)
	}
}

func listNextPage() {
	if !queueFocused {
		mainList.Transform(cview.TransformNextPage)
		go app.QueueUpdateDraw(updateMain)
	} else {
		queueList.Transform(cview.TransformNextPage)
		go app.QueueUpdateDraw(updateQueue)
	}
}

func listRefresh() {
	mainCursor = mainList.GetCurrentItemIndex()
	d := mainDirectory

	mainDirectory = ""
	browseFolder(d)
}

func listToggleHidden() {
	showHiddenFolders = !showHiddenFolders

	if showHiddenFolders {
		statusText = "Hidden folders: shown"
	} else {
		statusText = "Hidden folders: hidden"
	}
	go func() {
		time.Sleep(3 * time.Second)
		statusText = ""
		go app.QueueUpdateDraw(updateMain)
	}()

	listRefresh()
}

func toggleFocusedList() {
	queueFocused = !queueFocused

	focusUpdated(true)
}

func focusUpdated(setFocus bool) {
	if queueFocused {
		if setFocus {
			app.SetFocus(queueList)
		}

		mainList.SetSelectedTextColor(tcell.ColorWhite)
		queueList.SetSelectedTextColor(tcell.ColorBlack)

		mainList.SetSelectedTextAttributes(tcell.AttrBold | tcell.AttrUnderline)
		queueList.SetSelectedTextAttributes(0)

		mainList.SetSelectedBackgroundColor(tcell.ColorBlack)
		queueList.SetSelectedBackgroundColor(tcell.ColorWhite)
	} else {
		if setFocus {
			app.SetFocus(mainList)
		}

		mainList.SetSelectedTextColor(tcell.ColorBlack)
		queueList.SetSelectedTextColor(tcell.ColorWhite)

		mainList.SetSelectedTextAttributes(0)
		queueList.SetSelectedTextAttributes(tcell.AttrBold | tcell.AttrUnderline)

		mainList.SetSelectedBackgroundColor(tcell.ColorWhite)
		queueList.SetSelectedBackgroundColor(tcell.ColorBlack)
	}

	mainList.SetHighlightFullLine(!queueFocused)
	queueList.SetHighlightFullLine(queueFocused)

	if setFocus {
		go app.QueueUpdateDraw(updateLists)
	}
}
