package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gopkg.in/yaml.v2"
)

type appConfig struct {
	Input  map[string][]string // Keybinds
	Layout string
	Volume int // Starting volume
}

var config = &appConfig{
	Layout: defaultLayout,
	Volume: defaultVolume,
}

func defaultConfigPath() string {
	homedir, err := os.UserHomeDir()
	if err == nil && homedir != "" {
		return path.Join(homedir, ".config", "ditty", "config.yaml")
	}

	return ""
}

func readConfig(configPath string) error {
	if configPath == "" {
		configPath = defaultConfigPath()
		if configPath == "" {
			return nil
		} else if _, err := os.Stat(configPath); os.IsNotExist(err) {
			return nil
		}
	}

	configData, err := ioutil.ReadFile(configPath)
	if err != nil {
		return fmt.Errorf("failed to read file: %s", err)
	}

	err = yaml.Unmarshal(configData, config)
	if err == nil && (config.Volume < 0 || config.Volume > 100) {
		err = fmt.Errorf("invalid volume (must be between 0 and 100): %d", config.Volume)
	}
	if err != nil {
		return fmt.Errorf("failed to parse file: %s", err)
	}

	return nil
}
