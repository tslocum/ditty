module gitlab.com/tslocum/ditty

go 1.15

require (
	github.com/dhowden/tag v0.0.0-20201120070457-d52dcb253c63
	github.com/faiface/beep v1.0.3-0.20210301102329-98afada94bff
	github.com/gdamore/tcell/v2 v2.2.0
	github.com/hajimehoshi/go-mp3 v0.3.1 // indirect
	github.com/jfreymuth/oggvorbis v1.0.3 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.10
	github.com/mewkiz/flac v1.0.7 // indirect
	github.com/mewkiz/pkg v0.0.0-20210112042322-0b163ae15d52 // indirect
	gitlab.com/tslocum/cbind v0.1.4
	gitlab.com/tslocum/cview v1.5.3
	golang.org/x/exp v0.0.0-20210220032938-85be41e4509f // indirect
	golang.org/x/image v0.0.0-20210220032944-ac19c3e999fb // indirect
	golang.org/x/mobile v0.0.0-20210220033013-bdb1ca9a1e08 // indirect
	golang.org/x/sys v0.0.0-20210303074136-134d130e1a04 // indirect
	golang.org/x/term v0.0.0-20210220032956-6a3ed077a48d // indirect
	gopkg.in/yaml.v2 v2.4.0
)
